import tkinter as tk

count = 0
line = ""
n = "1"
x = 0
y = 0
exported = False
elems = []
tops = []
topt = {}
edges = []
answers = {}
answer_mode = {}
modes = {}
root = tk.Tk()
elms = tk.StringVar()
elms.set("Nothing yet")
var = tk.IntVar()
text = ""
selection = ""
token = ""
canvas = tk.Canvas(root, width=1000, height=1000, borderwidth=10, highlightthickness=0, bg="black") 
canvas.grid()
#for editing the file
def Add(filename, token, ifs, modes):
    global edges
    modetext = ""


    for k,v in modes.items():
        modetext = modetext + '''
            if(text == \"{message}\"):
                mode = \"{mode}\"

        '''.format(message = modes[k], mode = v)

    iftext = ""

    for v,k in ifs.items():
        mde = answer_mode[v]
        iftext = iftext + '''
            if(mode == \"{themode}\"):
                if(text == \"{message}\"):
                    send(event.user_id, \"{answer}\")
        \n
        '''.format(message = k.lower(), answer = v.lower(), themode = mde)

    fh = open(filename,"w")
    print(ifs)
    s = ''' 
import vk_api
from vk_api.longpoll import VkLongPoll, VkEventType
import random

mode = \"0\"

vk = vk_api.VkApi(token=\'{token}\')
longpoll = VkLongPoll(vk)

def send(user_id, message):
    vk.method('messages.send', {text})

for event in longpoll.listen():

            
    if(event.type == VkEventType.MESSAGE_NEW):
            
                
        if event.to_me:
  
            text = event.text
            text = text.lower()
            userid = event.user_id

            {modetext}

            {iftext}
        '''.format(token = tken.get(), modetext = modetext,iftext = iftext, text =  "{'user_id': user_id, 'message': message, 'random_id':random.randint(0,999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999)}")
    fh.write(s)
    fh.close()


def key(event):
    print("pressed")
#for creating the edge
def btn(event=None):
    p1 = top1.get()
    p2 = top2.get()
    if(p1 == p2):
        canvas.create_line(tops[int(p1) - 1][0] + 20,tops[int(p1) - 1][1] + 20, tops[int(p1) - 1][0],tops[int(p1) - 1][1] + 60, tops[int(p1) - 1][0] - 60,tops[int(p1) - 1][1]+60,tops[int(p1) - 1][0] - 20,tops[int(p1) - 1][1] + 20, arrow=tk.LAST, fill = "blue", activewidth = 10, width = 5, smooth = "true")
        edges.append([int(p1) - 1, int(p1) - 1])
    else:
        canvas.create_line(tops[int(p1) - 1][0] + 10,tops[int(p1) - 1][1] + 10 , tops[int(p2) - 1][0] - 10,tops[int(p2) - 1][1] - 10, arrow=tk.LAST, fill = "blue", activewidth = 10, width = 5)
    edges.append([int(p1) - 1, int(p2) - 1])
#for generating the code
def export(event=None):
    global line, text, elems, answers, answer_mode
    ifs = {}
    rows = []
    modes = {}

    for x in range(len(tops)):
        for y in range(len(tops)):
            if([x,y] in edges):
                line = line + "1"
            else:
                line = line + "0"
        rows.append(line)
        text = text + line + "\n"
        line = ""

    text = ""
    print(text)
    print(rows)

    i = 0
    u = 0
    print("rows: " + str(len(rows)))
    for i in range(len(rows)):
        for u in range(len(rows[int(i)])):
            print("number is: " + str(u))
            if(rows[int(i)][u] == "1"):
                print("is 1")
                
                if(isinstance(elems[u], answer)):
                    print("answers:")
                    answers[elems[u].message] = elems[u].answer
                    answer_mode[elems[u].message] = elems[u].mode

                    print(answers)  

                elif(isinstance(elems[u], mode)):
                    print("answers:")
                    modes[elems[u].message] = elems[u].mode

                    print(answers) 
                    
    
    ifs = answers
    print(ifs)
    Add(name.get(), token, ifs, modes)

    

    text = ""

#for creating the circle
def circle(x,y,r):
    canvas.create_circle(x, y, r, fill="red", width=1)

def _create_circle(self, x, y, r, **kwargs):
    return self.create_oval(x-r, y-r, x+r, y+r, **kwargs)
tk.Canvas.create_circle = _create_circle
#for creating nodes
def getorigin(eventorigin):
    global n, x, y, topt, topelems, selection, elems
    toptt = ""
    x = eventorigin.x
    y = eventorigin.y
    circle(x,y, 30)

    if(var.get() == 1):
        topt[str(n)] = "Answer"
    if(var.get() == 2):
        topt[str(n)] = "Setup"
    if(var.get() == 3):
        topt[str(n)] = "Mode"

    print(topt)
    canvas.create_text(x, y, text="" + "(" + n + ")", fill = "white", font = ("Purisa", 20))
    tops.append([x, y])
    n = str(int(n) + 1)
    for k in topt:
        toptt = toptt + str(k) + ", " + topt[k] + "\n"
    elms.set(toptt)
    
    if(var.get() == 1):
        selection = "answer"
    elif(var.get() == 2):
        selection = "setup"

    print(var.get())
    print(selection)

    if(selection == "setup"):
        sup = setup(tken.get())
        elems.append(sup)
        print(elems)
        print(elems[int(n) - 2].token)
        
    elif(selection == "answer"):
        asw = answer(anwe.get(), msge.get(), mod.get())
        elems.append(asw)
        print(elems)
        
    elif(selection == "mode"):
        mo = mode(msge.get(), mod.get())
        elems.append(mo)
        print(elems)

    toptt = ""
#clears all info. For some reason sometimes doesn't work (╯°□°）╯︵ ┻━┻
def delete(event=None):
    global tops, edges, x, y, line, count, n, topt, message, answer, elms, var
    topt = {}
    canvas.delete("all")
    tops = []
    edges = []
    count = 0
    line = ""
    n = "1"
    x = 0
    y = 0
    elems = []
    tops = []
    topt = {}
    edges = []
    message = "Hello"
    answer = "Hello, user!"
    elms = tk.StringVar()
    elms.set("Nothing yet")
    var = tk.IntVar()
    text = ""
    selection = ""
    token = "sample_token"
#checks the radio buttons
def sel():
    global selection
    if(var.get() == 1):
        selection = "answer"
    elif(var.get() == 2):
        selection = "setup"
    elif(var.get() == 3):
        selection = "mode"

    print(selection)
#class initialization
class setup:
    def __init__(self, token):
        self.token = token
class answer:
    def __init__(self, message, answer, mode):
        self.message = message
        self.answer = answer
        self.mode = mode
class mode:
    def __init__(self, message, mode):
        self.message = message
        self.mode = mode

#some GUI stuff ¯\_(ツ)_/¯
canvas.bind("<Key>", key)
canvas.bind("<Button-1>", getorigin)

frame = tk.Frame(root, bg='black', width=1000, height=40)
frame.pack(fill='x', side='top')
frame2 = tk.Frame(root, bg='black', width=40, height=1000)
frame2.pack(fill='x', side='bottom')
frame3 = tk.Frame(root, bg='black', width=40, height=1000)
frame3.pack(fill='x', side='right')
token_entry = tk.Entry(frame2, width=2)

button1 = tk.Button(frame, text='Add edge', command=btn)
button3 = tk.Button(frame, text='Delete', command=delete)
button3.pack(side='right', padx=10)
button1.pack(side='left', padx=10)

top1 = tk.Entry(frame, width=2)
top2 = tk.Entry(frame, width=2)
top1.pack(side='left', padx=10)
top2.pack(side='left', padx=10)

msge = tk.Entry(frame3, width=40)
msge.pack(side='bottom', padx=10 , anchor = "s")

label3 = tk.Label(frame3, font=("Helvetica", 10), text = "Message:")
label3.pack(side='bottom', anchor = "s")

anwe = tk.Entry(frame3, width=40)
anwe.pack(side='bottom', padx=10 , anchor = "s") 

label2 = tk.Label(frame3, font=("Helvetica", 10), text = "Answer:")
label2.pack(side='bottom', anchor = "s")

tken = tk.Entry(frame3, width=40)
tken.pack(side='bottom', padx=10 , anchor = "s")

label1 = tk.Label(frame3, font=("Helvetica", 10), text = "Token:")
label1.pack(side='bottom', anchor = "s")

mod = tk.Entry(frame3, width=40)
mod.pack(side='bottom', padx=10 , anchor = "s")

label4 = tk.Label(frame3, font=("Helvetica", 10), text = "Mode:")
label4.pack(side='bottom', anchor = "s")

name = tk.Entry(frame2, width=10)
name.pack(side='left', anchor = "s")

button2 = tk.Button(frame, text='Export', command=export)
button2.pack(side='left', padx=10)

R1 = tk.Radiobutton(frame2, text="Setup", variable=var, value=2, command=sel)
R1.pack(side='bottom', anchor = "sw")

R2 = tk.Radiobutton(frame2, text="Answer", variable=var, value=1,command=sel)
R2.pack(side="bottom", anchor = "sw")

R2 = tk.Radiobutton(frame2, text="Mode", variable=var, value=3,command=sel)
R2.pack(side="bottom", anchor = "sw")

elements = tk.Label(frame3, font=("Helvetica", 20), textvariable = elms, width = 35, height=22)
elements.pack(side="top", anchor = "n")



canvas.pack()
root.mainloop()













































#You found a secret
#( ͡ᵔ ͜ʖ ͡ᵔ )