# VisualBot

App for visual programming. Used to create vk bots for groups.
Requirements: Python 3.6, tkinter, vk_api.

Instructions:
1. Create a folder with the code.py file in it
2. Add another .py file in the folder
3. Launch code.py.
4. Create bot (Instructions in GUI.png and below)
5. Launch the other .py file you created and named in the app.

Node requirements(for creation):

Setup: token.   

Answer: mode, message, answer.  

Mode: mode, message.    


Each answer type is connected FROM the mode node TO itself with an edge. 
Each mode type is connected FROM the setup node TO intself with an edge.
There should be only ONE setup node, otherwise there could be problems with the token.



GUI V1.0:
1 - Create an edge (From first node to the second node)
2 - First node (For edge)
3 - Second node (For edge)
4 - Export code to the .py file
5 - .py file name
6 - Node type
7 - Elements
8 - Input for node creation

Setup node:
Initializes the token.

Answer node:
Runs only in the right mode. Answers the "message" with the "answer".

Mode node:
Turns on a mode if the user sends the "message".